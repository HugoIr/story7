from django.test import TestCase, Client
from django.urls import resolve
from .views import statusan, tambah_status
from .models import Statusan
from .forms import Statusan_Form
from django.utils import timezone

class StatusanUnitTest(TestCase):
    
    def test_statusan_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_statusan_using_statusan_func(self):
        got = resolve('/')
        self.assertEqual(got.func, statusan)
    
    def test_statusan_using_tambah_status_func(self):
        got = resolve('/tambah-status/nama/status')
        self.assertEqual(got.func, tambah_status)

    def test_direct_url_tambah_status_redirect_to_homepage(self):
        got = Client().get('/tambah-status')
        self.assertEqual(got.status_code,404)

    def test_create_new_status(self):
        new_status = Statusan.objects.create(nama="hugs", status="bosen nich", warna='danger')
        counting_all_status = Statusan.objects.all().count()
        self.assertEqual(counting_all_status, 1)
    
    def test_statusan_form_has_placehoder(self):
        form = Statusan_Form()
        self.assertIn('placeholder="Masukkan nama...', form.as_p())
        self.assertIn('placeholder="Masukkan statusmu...', form.as_p())

    def test_statusan_form_for_blank_status(self):
        form = Statusan_Form(data={'nama':'hugi', 'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ['This field is required.'])
        
    # Untuk test __str__ di models.py
    def test_statusan_objects_has_nama(self):
        Statusan.objects.create(nama="coco", status="saya nata de coco", warna='danger')
        object1 = Statusan.objects.get(nama="coco")
        self.assertEqual(str(object1),object1.nama)



from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
# class StatusanFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options= Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')        
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(StatusanFunctionalTest, self).setUp()
    
#     def tearDown(self):
#         self.selenium.quit()
#         super(StatusanFunctionalTest, self).tearDown()
    

#     def test_input_status_with_cancel_confirmation(self):

#         selenium = self.selenium
#         openUrl = selenium.get(self.live_server_url + '/')

    #     # find form element
    #     nama = selenium.find_element_by_id('id_nama')
    #     status = selenium.find_element_by_id('id_status')
    #     submit = selenium.find_element_by_id('submit')

    #     #fill the form
    #     nama.send_keys("adudu")
    #     status.send_keys("gue lagi nyoba masukkin tapi sengaja digagalin di functional test input otomatis inihhh")

    #     #submit the form
    #     submit.click()

    #     # On the confirmation page
    #     cancel = selenium.find_element_by_id('cancel')
    #     cancel.click()

    #     # Check if the input is not created
    #     gotPage = selenium.page_source
    #     assert 'adudu' not in gotPage
    #     assert 'gue lagi nyoba masukkin tapi sengaja digagalin di functional test input otomatis inihhh' not in gotPage        

    # def test_input_with_yes_confirmation(self):
    #     selenium = self.selenium
    #     openUrl = selenium.get(self.live_server_url + '/')
        
    #     # find form element
    #     nama = selenium.find_element_by_id('id_nama')
    #     status = selenium.find_element_by_id('id_status')
    #     submit = selenium.find_element_by_id('submit')

    #     #fill the form
    #     nama.send_keys("hugas")
    #     status.send_keys("gue lagi nyoba masukkin functional test input otomatis")

    #     #submit the form
    #     submit.click()

    #     # On the confirmation page
    #     buttonYes = selenium.find_element_by_id('button-yes')
    #     buttonYes.click()

    #     # Check if the input is created
    #     gotPage = selenium.page_source
    #     assert 'hugas' in gotPage
    #     assert 'gue lagi nyoba masukkin functional test input otomatis' in gotPage

    # def test_input_name_blank_with_yes_confirmation(self):
    #     selenium = self.selenium
    #     openUrl = selenium.get(self.live_server_url + '/')
        
    #     # find form element
    #     nama = selenium.find_element_by_id('id_nama')
    #     status = selenium.find_element_by_id('id_status')
    #     submit = selenium.find_element_by_id('submit')

    #     #fill the form        
    #     status.send_keys("Lorem ipsum blablabla")
    #     #submit the form
    #     submit.click()

    #     # On the confirmation page
    #     buttonYes = selenium.find_element_by_id('button-yes')
    #     buttonYes.click()

    #     # Check if the input is created
    #     gotPage = selenium.page_source
    #     assert 'Anonim' in gotPage
    #     assert 'Lorem ipsum blablabla' in gotPage


    # def test_box_color_can_be_changed(self):
    #     # Create object so it's can be changed
    #     Statusan.objects.create(nama='lol',status='lagi challenge pepeew', warna='dark')
    #     selenium = self.selenium
    #     openUrl = selenium.get(self.live_server_url + '/')

    #     # Find and click color button
    #     color_button = selenium.find_element_by_name('color')
    #     color_button.click()

    #     # warna sebelumnya adalah dark, jika warna benar terganti 
    #     # maka part html tersebut seharusnya berubah
    #     assert '<div class="card text-white bg-dark mb-3">' not in selenium.page_source