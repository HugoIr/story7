from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from .forms import Statusan_Form
from .models import Statusan
import time
from django.utils import timezone
import random
from itertools import cycle

response={}

# Untuk Challenge mengganti warna box dimana warna 
# berikut akan dipassing sebagai warna pada class Bootstrap 4 di statusan.html
warna_list = ['success','info', 'warning', 'danger', 'secondary', 'primary','dark']
warna_cycle = cycle(warna_list)



def statusan(request):

    if not request.user.is_authenticated:
        return redirect('login/')
    # Membuat session expired dalam 300 detik (5 menit)
    request.session.set_expiry(300)

    response['status'] = Statusan.objects.all()
    response['status_form'] = Statusan_Form

    # Jika button ditekan warna akan terganti
    if(request.POST.get('color', False)):
        id = request.POST.get('color', False)
        obj = Statusan.objects.get(id=id)
        obj.warna = next(warna_cycle)
        obj.save()


    form = Statusan_Form(request.POST or None)
    # Jika form diisikan secara valid
    if(request.method == 'POST' and form.is_valid()):
        response['nama'] = request.POST['nama']
        response['status'] = request.POST['status']
        # Jika nama yang diisikan kosong
        if(response['nama'] == ''):
            response['nama'] = 'Anonim'
        return render(request, 'confirmation.html', response)
        
    return render(request, "statusan.html", response)
def tambah_status(request, nama, status):
    
    statusan = Statusan(nama=nama, status=status, warna=random.choice(warna_list))
    statusan.save()
    return redirect('statusan-yuk:statusan')
    



