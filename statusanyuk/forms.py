from django import forms

class Statusan_Form(forms.Form):
    nama_attrs = {
        'type': 'text',
        'class': 'form-control mb-4',
        'placeholder': 'Masukkan nama...'
    }
    status_attrs = {
        'type':'text',
        'class':'form-control',
        'placeholder':'Masukkan statusmu...'
    }

    nama = forms.CharField(label='', required=None,max_length=27, widget=forms.TextInput(attrs=nama_attrs))
    status = forms.CharField(label='', required=True,max_length=1000, widget=forms.Textarea(attrs=status_attrs))
