from django.db import models

# Create your models here.
class Statusan(models.Model):
    nama = models.CharField(max_length=40, null=True, blank=True)
    status = models.TextField()
    tanggal_buat = models.DateTimeField(auto_now_add=True)
    warna = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.nama