from django.urls import path
from django.conf.urls import url
from . import views

app_name = "statusan-yuk"

urlpatterns = [
    url(r'^$', views.statusan , name="statusan"),
    path('tambah-status/<str:nama>/<str:status>', views.tambah_status, name="tambah_status"),
]