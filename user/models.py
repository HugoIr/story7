from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete= models.CASCADE)
    fullname = models.CharField(max_length=100, null=True)
    date_birth = models.DateField(null=True)
    # image = models.ImageField(default='default.jpeg', upload_to='profile_pics')
    image = models.URLField(max_length=1000, null=True)
    def __str__(self):
        return f'{self.user.username} Profile'