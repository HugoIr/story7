from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth.decorators import login_required
from .models import Profile
from django.contrib.auth.models import User


# Create your views here.
def signup(request):
    
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            form = form.cleaned_data.get('username')
            messages.success(request, f'Selamat Anda sudah berhasil membuat akun!')
            return redirect('login')
        

    else:
        form = UserRegisterForm()


    return render(request, 'signup.html', {'form':form})

@login_required
def profile(request):
    try:
        pengguna = Profile.objects.get(user__username= request.user.username)
    except:
        pengguna = Profile.objects.filter(user__username= request.user.username)
    return render(request, 'profile.html', {'pengguna':pengguna})
