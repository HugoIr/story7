# Generated by Django 2.2 on 2020-05-08 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_auto_20200508_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.URLField(max_length=1000, null=True),
        ),
    ]
